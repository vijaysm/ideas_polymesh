/*
 * =====================================================================================
 *
 *       Filename:  ExtrudePolygons.hpp
 *
 *    Description:  Essential objects to handle the input options provided by user either
 *                  through command-line or through setup files that specify the layering
 *                  and material data information.
 *
 *        Version:  1.0
 *        Created:  02/22/2016 16:58:08
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vijay S. Mahadevan (vijaysm), mahadevan@anl.gov
 *        Company:  Argonne National Lab
 *
 * =====================================================================================
 */

// C++ includes
#include <iostream>
#include <string>
#include <sstream>

// GetPot include for input file options processing
#include "GetPot.hpp"

class PolygonExtrusionContext
{
private:
  int *argc;
  char ***argv;
  double defaultThickness;
  int totalCells;

public:
  std::string input_options_file_name;
  std::string input_file_name;
  std::string output_file_name;
  int nlayers;
  bool repeatFaceBlocks; // 0 for false, 1 for true
  // 1 option is to be used to view the mesh in paraview
  std::vector<double> layer_thickness;
  std::vector<int>    elements_per_layer;
  std::vector<int>    material_in_layer;
  int neumannSetValues[3] ; // there are 3 neumann sets created, one for first layer of polygs (top for negative height)
                            //                                   second for bottom layer (for neg heights)
                            //                                   lateral set (formed with quads)
  bool                verbose;

  PolygonExtrusionContext(int p_argc, char** p_argv) :
    argc(&p_argc),
    argv(&p_argv),
    defaultThickness(1.0),
    totalCells(0),
    input_options_file_name(""),
    input_file_name("input/poly_test.vtk"),
    output_file_name("output/polyhedra.vtk"),
    nlayers(1),
    repeatFaceBlocks(false),
    layer_thickness(0),
    elements_per_layer(0),
    material_in_layer(0),
    verbose(true)
  {
    // Look at the user options for handling the extrusion process
    // Take the input file with layering specifications as an argument
    // Also support constant mesh extrusion height for testing purposes
    ProgOptions opts;

    // Get options file name
    opts.addOpt<std::string>("opt,O",
                             "Input options file to control the extrusion process (default=)", &input_options_file_name);

    // Get input mesh file name
    opts.addOpt<std::string>("input,i",
                             "Input mesh file name (default=input/poly_test.vtk)", &input_file_name);

    // Get output mesh file name
    opts.addOpt<std::string>("output,o",
                             "Output mesh file name (default=output/polyhedra.vtk)", &output_file_name);

    // Get number of layers
    opts.addOpt<int>("layers,l",
                     "Number of layers to extrude the input 2-D polygonal mesh (default=1)", &nlayers);

    // Get number of layers
    opts.addOpt<double>("depth,d",
                        "Constant thickness of the layers to extrude the input 2-D polygonal mesh (default=1.0)", &defaultThickness);

    // Get repeat options for face blocks
    opts.addOpt<void>("repeat,r",
                     "Repeat face blocks for visualization in Paraview/VisIt (default=false)", &repeatFaceBlocks);

    // Get whether output is verbose
    bool silent = false;
    opts.addOpt<void>("silent,s",
                      "Disable verbose output during the run (default=false)", &silent);

    opts.parseCommandLine(p_argc, p_argv);
    verbose = !silent;
  }

  ~PolygonExtrusionContext() {
    argc = 0;
    argv = 0;
    defaultThickness = 0.0;
    totalCells = 0;
    nlayers = 0;
    layer_thickness.clear();
    elements_per_layer.clear();
    material_in_layer.clear();
  }

  unsigned totalAxialCells()
  {
    if (!totalCells) {
      totalCells=0;
      // sum of all cells in layers
      for (int i=0; i < nlayers; ++i) {
        totalCells += elements_per_layer[i];
      }

      if (verbose)
        std::cout << "Number of axial cells per polygon = " << totalCells << std::endl;
    }
    return totalCells;
  }

  void printOptions()
  {
    std::cout << "Current Run Parameters::" << std::endl;
    std::cout << "------------------------" << std::endl;;
    if (input_options_file_name.length()) { // User specified an input options file
      std::cout <<  "Input Run Options FileName = " << input_options_file_name << std::endl;
    }

    std::cout << "Input Mesh FileName = " << input_file_name << std::endl;
    std::cout << "Output Mesh FileName = " << output_file_name << std::endl;
    std::cout << "Total Number of Layers = " << nlayers << std::endl;
    std::cout << "Neumann set values: " << neumannSetValues[0] << " " << neumannSetValues[1] << " " << neumannSetValues[2] << std::endl;
    for (int i=0; i < nlayers; ++i)
    {
      std::cout << "\tLayer[" << i << "] -- Elements/Layer = " << elements_per_layer[i] << " Thickness/Element = " << layer_thickness[i] << " Material_ID = " << material_in_layer[i] << std::endl;
    }
    totalAxialCells();
    std::cout << "Repeat face blocks (for visualization purposes) = " << (repeatFaceBlocks ? "true":"false") << std::endl;
    std::cout << std::endl;
  }

  void initializeContext()
  {
    if (input_options_file_name.size()) { // User specified an input options file
      GetPot  optFile(input_options_file_name.c_str());
      // optFile.print();

      nlayers = optFile("common/nlayers",1);
      input_file_name = optFile("common/input_mesh",input_file_name.c_str());
      output_file_name = optFile("common/output_mesh",output_file_name.c_str());
      neumannSetValues[0] = optFile("common/first_set", 10);
      neumannSetValues[1] = optFile("common/last_set", 20);
      neumannSetValues[2] = optFile("common/lateral_set", 30);

      if (!repeatFaceBlocks) {
        int irepeatFaceBlocks = optFile("common/repeat_face_blocks", 0);
        repeatFaceBlocks = (irepeatFaceBlocks != 0);
      }

      // Resize all the vectors
      elements_per_layer.resize(nlayers);
      layer_thickness.resize(nlayers);
      material_in_layer.resize(nlayers);

      std::stringstream sstr;
      for (int i=0; i < nlayers; ++i)
      {
        sstr << i+1 << "/ncells";
        std::string ilayer_elems = sstr.str();
        sstr.str("");
        sstr << i+1 << "/thickness";
        std::string ilayer_thick = sstr.str();
        sstr.str("");
        sstr << i+1 << "/material_id";
        std::string ilayer_material = sstr.str();
        sstr.str("");

        elements_per_layer[i] = optFile(ilayer_elems.c_str(), 1);
        layer_thickness[i]    = optFile(ilayer_thick.c_str(), 1.0);
        material_in_layer[i]  = optFile(ilayer_material.c_str(), -1);
      }
    }
    else {
      elements_per_layer.resize(nlayers);
      layer_thickness.resize(nlayers);
      material_in_layer.resize(nlayers);
      for (int i=0; i < nlayers; ++i)
      {
        elements_per_layer[i] = 5;
        layer_thickness[i] = defaultThickness;
        material_in_layer[i] = 1;
      }
    }

    if (verbose)
      this->printOptions();
  }

};
