/** 
 * =====================================================================================
 * @example ExtrudePolygons.cpp \n
 *
 * Description: Read a 2-d polygonal mesh that is defined on a plane and then extrude it
 *              to form multiple layers of prisms (polyhedra) with appropriate property
 *              sets (material and boundary conditions) propagated through user input.\n
 *
 * To run: ./ExtrudePoly [meshfile] [outfile] [nlayers] [thickness_layer] \n
 * 
 * Usage: ExtrudePolygons --help | [options] \n
 *    Options: \n
 *      -h [--help]       : Show full help text \n
 *      -O [--opt] <arg>  : Input options file to control the extrusion process (default=) \n
 *      -i [--input] <arg>: Input mesh file name (default=input/poly_test.vtk) \n
 *      -o [--output] <arg>: Output mesh file name (default=output/polyhedra.vtk) \n
 *      -l [--layers] <int>: Number of layers to extrude the input 2-D polygonal mesh (default=1) \n
 *      -d [--depth] <val>: Constant thickness of the layers to extrude the input 2-D polygonal mesh (default=1.0) \n
 *
 * Sample usage:
 *
 *   1) ./ExtrudePolygons -O sample_run.inp
 *      Info -> Here sample_run.inp is the input file specifying the layer extrusion options.
 *   2) ./ExtrudePolygons -i input/poly_test.vtk -o output/output_test.vtk -l 4 -d 0.25
 *      Info -> -i input_mesh_file_name -o output_mesh_file_name -l number_of_layers -d layer_thickness
 *
 *        Version:  1.0
 *        Created:  02/22/2016 16:58:08
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Authors:  Vijay S. Mahadevan (vijaysm), mahadevan@anl.gov
 *         Authors:  Iulian Grindeanu (grindeanu), iulian@anl.gov
 *        Company:  Argonne National Lab
 * =====================================================================================
 */

// MOAB includes
#include "moab/Core.hpp"
#include "moab/ReadUtilIface.hpp"
#include "moab/ProgOptions.hpp"

// Tool includes
#include "ExtrudePolygons.hpp"

using namespace moab;

// Specify maximum number of polygonal edges to support
// Current default is at 20
#define MAXEDGES  20

int main(int argc, char **argv)
{
  ErrorCode rval;
  PolygonExtrusionContext ctx(argc, argv);
  
  if (argc < 3) { // At a minimum, we expect the user to provide an options file
    std::cout << "Usage: \n\t ./ExtrudePoly -O input_options_file_name \n\t ./ExtrudePoly -i input_mesh_file_name -o output_mesh_file_name -l number_of_layers -d layer_thickness\n";
    return 1;
  }

  ctx.initializeContext();

  // Get MOAB instance
  Interface* mb = new (std::nothrow) Core;
  if (NULL == mb)
    return 1;

  // Load the mesh from vtk file
  rval = mb->load_mesh(ctx.input_file_name.c_str());MB_CHK_ERR(rval);

  // Get verts entities, by type
  Range verts, faces, edges;

  // Get all the vertices recursively
  rval = mb->get_entities_by_type(0, MBVERTEX, verts);MB_CHK_ERR(rval);

  // Get faces, by dimension, so we stay generic to entity type
  rval = mb->get_entities_by_dimension(0, 2, faces);MB_CHK_ERR(rval);
  
  EntityHandle neuSets[3];
  // create 3 Neumann sets, for top, bottom and lateral set
  for (int k=0; k<3; k++)
  {
    rval = mb->create_meshset(MESHSET_SET, neuSets[k]);MB_CHK_ERR(rval);
  }
  // add the initial faces to the first set
  rval = mb->add_entities(neuSets[0], faces); MB_CHK_ERR(rval);
  // Create all edges
  rval = mb->get_adjacencies(faces, 1, true, edges, Interface::UNION);MB_CHK_ERR(rval);

  if (ctx.verbose) {
    std::cout << "Input Polygonal Mesh details::" << std::endl;
    std::cout << "\tNumber of Vertices = " << verts.size() << std::endl;
    std::cout << "\t          Edges    = " << edges.size() << std::endl;
    std::cout << "\t          Faces    = " << faces.size() << std::endl;
  }

  std::vector<double> coords;
  unsigned nvPerLayer= verts.size();
  coords.resize(3*nvPerLayer);

  // get the vertex coordinates for the polygonal mesh
  rval = mb->get_coords(verts, &coords[0]); MB_CHK_ERR(rval);

  // create first vertices
  unsigned ntotalElems = ctx.totalAxialCells();
  Range * newVerts = new Range [ntotalElems+1];
  newVerts[0] = verts; // just for convenience
  for (int ii=0, ilayer=0; ii<ctx.nlayers; ii++) {
    for (int jj=0; jj<ctx.elements_per_layer[ii]; jj++, ++ilayer)
    {
      for (unsigned i=0;i<nvPerLayer; i++)
        coords[3*i+2] += ctx.layer_thickness[ii]; // Subtract or Add depending on the z-Direction to extrude

      rval = mb->create_vertices(&coords[0], nvPerLayer, newVerts[ilayer+1]); MB_CHK_ERR(rval);
    }
  }
    
  // for each edge, we will create layers quads
  unsigned nquads = edges.size()*ntotalElems;
  ReadUtilIface *read_iface;
  rval = mb->query_interface(read_iface);MB_CHK_SET_ERR(rval, "Error in query_interface");

  EntityHandle start_elem, *connect;

  // Create quads
  rval = read_iface->get_element_connect(nquads, 4, MBQUAD, 0, start_elem, connect);MB_CHK_SET_ERR(rval, "Error in get_element_connect");
  unsigned nedges = edges.size();

  int indexConn=0;
  for (unsigned j=0; j<nedges; j++)
  {
    EntityHandle edge=edges[j];

    const EntityHandle *conn2 = NULL;
    int num_nodes;
    rval = mb->get_connectivity( edge, conn2, num_nodes);MB_CHK_ERR(rval);
    if ( 2 != num_nodes) MB_CHK_ERR(MB_FAILURE);

    // see if the edge is on boundary; if yes, add the corresponding quads to the 3rd neumann set
    Range adjFacesToEdge;
    rval = mb->get_adjacencies(&edge, 1, 2, false, adjFacesToEdge); MB_CHK_ERR(rval);
    if (adjFacesToEdge.size()==1) //
    {
      Range lateralQuads(start_elem+indexConn/4, start_elem+indexConn/4+ntotalElems-1);
      rval = mb->add_entities(neuSets[2], lateralQuads); MB_CHK_ERR(rval);
    }

    int i0 = verts.index(conn2[0]);
    int i1 = verts.index(conn2[1]);
    for  (unsigned ii=0; ii<ntotalElems; ii++)
    {
      connect[indexConn++] = newVerts[ii][i0];
      connect[indexConn++] = newVerts[ii][i1];
      connect[indexConn++] = newVerts[ii+1][i1];
      connect[indexConn++] = newVerts[ii+1][i0];
    }
  }

  // Create the material ID tag
  int defmatid=-1, zero=0;
  moab::Tag materialTag, msetTag, neuTag, gidTag;
  rval = mb->tag_get_handle("MATERIALS", 1, MB_TYPE_INTEGER, materialTag, MB_TAG_CREAT|MB_TAG_DENSE, &defmatid);MB_CHK_ERR(rval);
  rval = mb->tag_get_handle("MATERIAL_SET", 1, MB_TYPE_INTEGER, msetTag, MB_TAG_CREAT|MB_TAG_SPARSE, &defmatid);MB_CHK_ERR(rval);
  rval = mb->tag_get_handle("NEUMANN_SET", 1, MB_TYPE_INTEGER, neuTag, MB_TAG_CREAT|MB_TAG_SPARSE, &defmatid);MB_CHK_ERR(rval);
  rval = mb->tag_get_handle("GLOBAL_ID", 1, MB_TYPE_INTEGER, gidTag, MB_TAG_DENSE | MB_TAG_CREAT, &zero);
  std::vector< EntityHandle>  blocks(ctx.nlayers);
  for (int kk=0; kk<ctx.nlayers; kk++) {
    // Get the material ID for layer kk
    int matid = ctx.material_in_layer[kk];
    EntityHandle block_handle;
    rval = mb->create_meshset(MESHSET_SET, block_handle); MB_CHK_ERR(rval);
    rval = mb->tag_set_data(msetTag, &block_handle, 1, &matid);
    blocks[kk] = block_handle;
  }
  unsigned int nfaces = faces.size();
  std::vector<EntityHandle> allPolygons(nfaces*(ntotalElems+1));
  for (unsigned int i=0; i<nfaces; i++)
    allPolygons[i] = faces[i];

  for (unsigned int j=0; j<nfaces; j++)
  {
    // vertices are parallel to the base vertices
    // polygons with at most MAXEDGES edges
    // edges will be used to determine the lateral faces of polyhedra (prisms)
    std::vector<int> indexVerts(MAXEDGES,0);
    std::vector<EntityHandle> newConn(MAXEDGES,0);
    std::vector<EntityHandle> polyhedronConn(MAXEDGES+2,0);
    std::vector<int> indexEdges(MAXEDGES, 0); // index of edges in base polygon
    
    EntityHandle polyg=faces[j];

    const EntityHandle *connp = NULL;
    int num_nodes;
    rval = mb->get_connectivity( polyg, connp, num_nodes);MB_CHK_ERR(rval);

    for (int i=0; i<num_nodes; i++)
    {
      indexVerts[i] = verts.index(connp[i]);
      int i1 =(i+1)%num_nodes;
      EntityHandle edgeVerts[2]={connp[i], connp[i1]};

      // get edge adjacent to these vertices
      Range adjEdges;
      rval = mb->get_adjacencies(edgeVerts, 2, 1, false, adjEdges);MB_CHK_ERR(rval);
      if (adjEdges.size()<1)
        MB_CHK_SET_ERR(MB_FAILURE, "Did not find any adjacent edges");
      indexEdges[i]=edges.index(adjEdges[0]);
      if (indexEdges[i]<0)
        MB_CHK_SET_ERR(MB_FAILURE, "Did not find any edges in the range");
    }

    for (int kk=0, ilayer=0; kk<ctx.nlayers; kk++) {
      // Get the material ID for this element, based on the data provided for the current layer
      int matid = ctx.material_in_layer[kk];
      EntityHandle block_handle = blocks[kk];
      // create material sets that will correspond to blocks in exodus files

      for (int jj=0; jj<ctx.elements_per_layer[kk]; jj++, ++ilayer)
      {
        // create a polygon on each layer
        for (int i=0; i<num_nodes; i++)
          newConn[i] = newVerts[ilayer+1][indexVerts[i]]; // vertices in layer ilayer+1

        rval = mb->create_element(MBPOLYGON, &newConn[0], num_nodes, allPolygons[nfaces*(ilayer+1)+j] ); MB_CHK_ERR(rval);

        // now create a polyhedra with top, bottom and lateral swept faces
        // first face is the bottom
        polyhedronConn[0] = allPolygons[nfaces*ilayer+j];
        // second face is the top
        polyhedronConn[1] = allPolygons[nfaces*(ilayer+1)+j];

        // next add lateral quads, in order of edges, using the start_elem
        // first layer of quads has EntityHandle from start_elem to start_elem+nedges-1
        // second layer of quads has EntityHandle from start_elem + nedges to start_elem+2*nedges-1 ,etc
        for (int i=0; i<num_nodes; i++)
        {
          polyhedronConn[2+i] = start_elem + ilayer + ntotalElems * indexEdges[i];
        }

        // Create polyhedron
        EntityHandle polyhedron;
        rval = mb->create_element( MBPOLYHEDRON, &polyhedronConn[0], 2+num_nodes, polyhedron); MB_CHK_ERR(rval);

        rval = mb->tag_set_data(materialTag, &polyhedron, 1, &matid);MB_CHK_ERR(rval);
        // add the polyhedron to the block

        rval = mb->add_entities(block_handle, &polyhedron, 1);MB_CHK_ERR(rval);
        std::vector<int> tmpMatIDs(num_nodes+2,matid);
        rval = mb->tag_set_data(materialTag, &polyhedronConn[0], 2+num_nodes, &tmpMatIDs[0]);MB_CHK_ERR(rval);
      }
    }
  }

  // add to the second neumann set the last layer of polygons
  // std::vector<EntityHandle> allPolygons(nfaces*(ntotalElems+1)) are all polygons
  // the last layer starts at allPolygons[nfaces*ntotalElems], size nfaces
  rval = mb->add_entities(neuSets[1], &allPolygons[nfaces*ntotalElems], nfaces); MB_CHK_ERR(rval);
  // now give some values to the neumann sets; 100, 200 and 300
  int val1 = ctx.neumannSetValues[0];
  rval = mb->tag_set_data(neuTag, &neuSets[0], 1, &val1); MB_CHK_ERR(rval);
  val1 = ctx.neumannSetValues[1];
  rval = mb->tag_set_data(neuTag, &neuSets[1], 1, &val1); MB_CHK_ERR(rval);
  val1 = ctx.neumannSetValues[2];
  rval = mb->tag_set_data(neuTag, &neuSets[2], 1, &val1); MB_CHK_ERR(rval);

  // give global ids to polyhedra, they will be used for side sets;
  // the order will be the same as the order in the file, by ranges of polyhedra
  int gid = 0;
  for (int kk=0; kk<(int)blocks.size(); kk++)
  {
    EntityHandle blockh = blocks[kk];
    Range polyhs;
    rval = mb->get_entities_by_handle(blockh, polyhs, true); MB_CHK_ERR(rval);
    for (Range::iterator pit =polyhs.begin(); pit!= polyhs.end(); pit++ )
    {
      gid++;
      EntityHandle polyh=*pit;
      rval = mb->tag_set_data(gidTag, &polyh, 1, &gid ); MB_CHK_ERR(rval);
    }
  }
  // Write the polyhedra to file
  std::cout << std::endl << "Writing extruded polyhedra mesh to " << ctx.output_file_name << std::endl;
  std::string options;
  if (ctx.repeatFaceBlocks>0)
    options="REPEAT_FACE_BLOCKS;";
  rval = mb->write_file(ctx.output_file_name.c_str(), 0, options.c_str());MB_CHK_ERR(rval);

  // Clean up
  delete mb;
  return 0;
}

