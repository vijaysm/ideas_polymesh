# MOAB_DIR points to top-level install dir, below which MOAB's lib/ and include/ are located
MOAB_DIR := ${PWD}/moab/build

ifneq ($(wildcard ${MOAB_DIR}/lib/moab.make),)
include ${MOAB_DIR}/lib/moab.make
include ${MOAB_DIR}/lib/iMesh-Defs.inc
MOAB_DEV = no
else
include ${MOAB_DIR}/moab.make
include ${MOAB_DIR}/itaps/imesh/iMesh-Defs.inc
MOAB_DEV = yes
endif

.SUFFIXES: .o .cpp

ALLEXAMPLES = ExtrudePolygons

default: all
all: $(ALLEXAMPLES)

ExtrudePolygons: src/ExtrudePolygons.o src/ExtrudePolygons.hpp ${MOAB_LIBDIR}/libMOAB.la
	@echo "Linking $?"
	@${MOAB_CXX} -o $@ $< ${MOAB_LIBS_LINK}

run-ExtrudePolygons: ExtrudePolygons
	@echo "Running: ./ExtrudePolygons -i input/poly_test.vtk -o output/output_simple_test.vtk -l 5 -d 0.2"
	@./ExtrudePolygons -i input/poly_test.vtk -o output/output_simple_test.vtk -l 5 -d 0.2 -s
	@echo "Running: ./ExtrudePolygons -O setups/sample_input.inp"
	@./ExtrudePolygons -O setups/sample_input.inp -s

check-ExtrudePolygons: run-ExtrudePolygons
	-@diff output/reference/output_simple_test.vtk.ref output/output_simple_test.vtk || printf "${PWD}\nPossible problem with ExtrudePolygons, diffs above\n=========================================\n";
	-@diff output/reference/output_poly_test.vtk.ref output/output_poly_test.vtk || printf "${PWD}\nPossible problem with ExtrudePolygons, diffs above\n=========================================\n";

src/%.o: src/%.cpp
	@echo "Compiling $?"
	@${MOAB_CXX} ${CXXFLAGS} ${MOAB_CXXFLAGS} ${MOAB_CPPFLAGS} ${MOAB_INCLUDES} -c $? -o $@

run: run-ExtrudePolygons
check: check-ExtrudePolygons

clean:
	rm -rf src/*.o ${ALLEXAMPLES}

